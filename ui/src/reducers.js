import { combineReducers } from 'redux'

const userReducer = (state, action) => {
  switch(action.type) {
    case 'login': return action.user
    case 'logout': return null
    default: return state || null
  }
}

const modalReducer = (state, action) => {
  switch(action.type) {
    case 'showModal': return { type: action.modalType }
    case 'hideModal': return null
    default: return state || null
  }
}

const productReducer = (state, action) =>
  action.type === 'products' ? action.products : state || null

  const temaReducer = (state, action) =>
  action.type === 'tema' ? action.tema : state || 'dark'

  // const registroReducer = (state, action) =>
  // action.type === 'registro' ? action.registro : state || null

  const visitReducer = (state, action) =>
  action.type === 'visit' ? state + 1 : state || 0

const rootReducer = combineReducers({
  //registro :registroReducer,
  user: userReducer,
  modal: modalReducer,
  products: productReducer,
  tema: temaReducer,
  visits: visitReducer
})

export default rootReducer











