
import React from 'react'
import Header from './Header';
import Slider from './Slider';
import '../assets/css/Home_styles.css';


const Home = () => {
  return (
    <div className="home">
      <header className="header">
        <Header />
      </header>
      <section>
        <Slider />
      </section>
    </div>
  )
}

export default Home
