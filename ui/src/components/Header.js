import React from 'react';
import logo from '../assets/images/logo.png';
import { createStore, applyMiddleware } from 'redux'
import { save, load } from "redux-localstorage-simple"
import { Provider,  useDispatch } from 'react-redux'
import rootReducer from '../reducers'
import { BrowserRouter as Router, } from 'react-router-dom'
import Modals from './modal/Modals'
import audio from '../assets/audio/s1.mp3'


const Contenido = () => {

    const dispatch = useDispatch()
    const handleRegistro = () => dispatch({ type: 'showModal', modalType: 'registro' })
    const handleLogin = () => dispatch({ type: 'showModal', modalType: 'login' })
    
    return (

        <header>
            <div className="logo">
                <img src={logo} width="80px" alt="logo" />
                <h1 className="text-logo">ZaKin Boss</h1>
            </div>
            <div>
                <audio src={audio} autoPlay loop controls/> 
            </div>
            <nav className="nav-items">
                <button onClick={handleRegistro} className="nav-item" >Registro</button> 
                <button onClick={handleLogin} className="nav-item" >Login</button>
                <Modals />       
            </nav>
        </header>
    )
};

const store = createStore(rootReducer, load(), applyMiddleware(save()))

const Header = () => {
    return (

        < Provider store={store} >
            <Router>
                <Contenido />
            </Router>

        </Provider >
    )
}


export default Header;


