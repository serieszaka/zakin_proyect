import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './Home'
import Error from './Error';
import Perfil from './Perfil';

const Rout = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/perfil">
                    <Perfil />
                </Route>                
                <Route component={Error} />
            </Switch>
        </Router>
    )

}
export default Rout;