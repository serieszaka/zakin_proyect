import React from 'react';
//import {useDispatch} from 'react-redux'
import logo from '../assets/images/logo.png';


const HeaderUser = () => {

    // const dispatch = useDispatch()
    // const logout = () => dispatch({ type: 'logout'})

    return (

        <header>
            <div className="logo">
                <img src={logo} width="80px" alt="logo" />
                <h1 className="text-logo">Three nights</h1>
            </div>
            <nav className="nav-items">                           
                
                {/* <button onClick = {logout} className="nav-item" id="item1"> Logout </button> */}

            </nav>
        </header>
    )
};

export default HeaderUser;
