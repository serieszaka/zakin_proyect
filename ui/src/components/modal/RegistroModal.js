import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import './Modals.css'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'


const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
}

const RegistroModal = () => {
    const [name, setName] = useFormField()
    const [birthday, setBirthday] = useFormField()
    const [password, setPassword] = useFormField()
    const [email, setEmail] = useFormField()
    // const [avatar, setAvatar] = useFormField()

    const history = useHistory()
    const [isError, setError] = useState(false)

    const handleSubmit = async (e) => {
        e.preventDefault()
        const user = { name, password, birthday, email }  //avatar
        setError(false)
        try {
            const ret = await fetch('http://localhost:8080/registro', {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem('token') // Esto en todas las llamadas autenticadas

                }
            })
            const data = await ret.json()
            //localStorage.setItem('token', data.token) // Esto solo en login, para guardar el token

            history.push(`/users/${data.id}`)
        } catch (err) {
            console.warn('Error:', err)
            setError(true)
        }
    }
    const dispatch = useDispatch()

    const handleClose = () => dispatch({ type: 'hideModal' })
    return (
        <form className="container">

            <div className="container-registro">

                <h1>Registro</h1>

                <div className="form_registro" onSubmit={handleSubmit}>
                    <Link to="/" className="btn-close-popup" onClick={handleClose}>X</Link>

                    <label>
                        Nombre: <br /><input name="username" required value={name} onChange={setName} placeholder="Nombre de usuario" />
                    </label>

                    <label>
                        F.nacimiento: <br /><input name="birthday" type="date" required value={birthday} onChange={setBirthday} placeholder="Fecha de nacimiento" />
                    </label>

                    <label>
                        Contraseña: <br /><input name="password" type="password" required value={password} onChange={setPassword} placeholder="Contraseña" />
                    </label>

                    <label>
                        Email: <br /><input name="email" type="email" required value={email} onChange={setEmail} placeholder="Correo" />
                    </label>

                    <button type="submit" className="button-registro" id="item1"> Registrarse </button>
                    {/* {status} */}
                </div>



            </div>


            {isError && <div>Error, please try again</div>}
        </form>
    )
}

export default RegistroModal;