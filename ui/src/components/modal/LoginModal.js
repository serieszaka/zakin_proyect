import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import './Modals.css'
import { Link } from 'react-router-dom'




const LoginModal = () => {
  const dispatch = useDispatch()
  const login = (user) => dispatch({ type: 'login', user })
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const handleSubmit = (e) => {
    e.preventDefault()
    // Aquí haríamos fetch para hacer login, y
    // obtendríamos los datos del user y un token...
    console.log('Login:', username, password)
    login({
      username,
      email: 'demo-email@spamherelots.com',
      avatar: 'https://i.imgur.com/VVq6KcT.png',
      token: 'fake-token'
    })
  }

  const handleClose = () => dispatch({ type: 'hideModal' })

  return (

    <form className="container">

      <div className="container-login">

        <h1>login</h1>

        <div className="login-form" onSubmit={handleSubmit}>
          <Link to="/" className="btn-close-popup" onClick={handleClose}>X</Link>

          <label>
            Username: <br /><input name="username" required value={username} onChange={e => setUsername(e.target.value)} placeholder="Nombre de usuario"/>
          </label><br />
          <label>
            Password: <br /><input name="password" type="password" required value={password} onChange={e => setPassword(e.target.value)} placeholder="Contraseña" />
          </label><br />
          <button className="button-login">Log in!</button>

        </div>
      </div>
    </form>

  )
}

export default LoginModal

