import React from 'react';

const Error = () => {
    return(
        <section id = "error">
            <h2 className = "error">Página no encontrada</h2>
            <p> La página a la que intentas acceder no se existe en la web </p>
        </section>
    )
}
export default Error;